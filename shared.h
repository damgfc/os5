#ifndef SHARED_H
#define SHARED_H

#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <semaphore.h>

#define PROCESSES 18
#define RESOURCES 20

typedef struct {
	int avail[RESOURCES]; // Available Vector
	int resource[RESOURCES]; // Resource Vector
	int executed[PROCESSES]; // Completed Processes
} resDescriptor; 

typedef struct {
	//Process Info
	int EMPTY_PROCESS_SLOT; // 0 if Process active. 1 if ready to take process.
	int FLAG_OSS; // set to 1 if need to alert OSS
	pid_t pid;
	unsigned int startSeconds;
	unsigned int startMilliseconds;
	
	//Resources
	int max[RESOURCES];
	int alloc[RESOURCES];
	int need[RESOURCES];
} process;

typedef struct {
	int pidArray[21];
	int processes;
	sem_t clockSemaphore;
	unsigned int clockSeconds;  //in seconds
	unsigned int clockMilliseconds; //in milliseconds
	resDescriptor descriptor;
	process proc[PROCESSES];
} sharedStruct;

int initSharedMemory();
void incrementTime(unsigned int seconds, unsigned int milliseconds);
void writeToLog(char *filename, char *string);
char *getCurrentTime();
void signalHandling(int sigNum, int array[], int size);
int detachAndRemove(int shmid, void *shmaddr);
void removeLogFiles();
#endif
