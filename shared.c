/*
$Author: o-moring $
$Log: shared.c,v $
Revision 1.1  2015/04/16 11:42:52  o-moring
Initial revision

$Date: 2015/04/16 11:42:52 $
*/

/******************/
/** Derek Moring **/
/**   Project 5  **/
/**   Shared.c   **/
/******************/
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <sys/shm.h>
#include "shared.h"

sharedStruct *shmP;

int initSharedMemory(){
	int key = ftok("./ftokFile", 42);
	if (key == -1){
		perror("ftok failed\n");
		return -1;
	}
	
	int shmid = shmget(key, sizeof(sharedStruct), 0777);
	
	if (shmid == -1){
		perror("Unsuccesful Shmget");
		exit(1);
	}
	else {
		shmP = (sharedStruct *)(shmat(shmid, 0, 0));
		if (shmP == (void *)-1){
			return -1;
		}
	}
}

char *getCurrentTime(){
	int SIZE = 15;
	char *timePt = (char *)malloc(SIZE);
	char returnTime[15];
	time_t currTime;
	struct tm * timeInfo;
  
  	time ( &currTime );
  	timeInfo = localtime ( &currTime );

	strftime(timePt, SIZE,"%T", timeInfo);
  	return timePt;
}

void writeToLog(char *filename, char *string){
FILE *fp;
char *timePt, time[10], output[250];
fp = fopen(filename, "a+");
if (fp == NULL){
	perror("Can't open File");
	exit(1);
}
timePt = getCurrentTime();

sprintf(output, "%s   %s\n", timePt, string);
fprintf(fp, output);

fclose(fp);
}

void signalHandling(int sigNum, int array[], int size){
	int i = 0;
	if (sigNum == SIGALRM){
		//kill all children
		for (i = 0; i < size; i++){
			kill(array[i], SIGINT);
		}
	}
	else if (sigNum == SIGINT){
		for (i = 0; i < size; i++){
			kill(array[i], SIGINT);
		}
	}
	else if (sigNum == SIGSEGV){
		for (i = 0; i < size; i++){
			kill(array[i], SIGINT);
		}
	}
	exit(sigNum);
}

int detachAndRemove(int shmid, void *shmaddr){
	int error = 0;
	
	if (shmdt(shmaddr) == -1){
		error = errno;
	}
	
	if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !error){
		error = errno;
	}
	
	if (!error){
		return 0;
	}
	
	errno = error;
	return -1;
}

void removeLogFiles(){
	int i = 0;
	char file[50];
	
	// Delete consumer log files
	for (i = 1; i < 19; i++){
		sprintf(file, "cons%02d.log", i);
 		if (remove(file) != 0){
			break;break;
		}
	}
	
	// Delete Producer log file
	remove("prod.log");

	// Delete Main log file
	remove("main.log");
}

/***  Random Num Generator  ***/
/*** Seeds and creates rand ***/
/***    num between range   ***/
int randomNumGen(int begin, int end){
    int num;
    srand(time(NULL));
    if (begin >= 0 && end >= 0){
        num = rand() % end + begin;
    }
    return num;
}

void incrementTime(unsigned int seconds, unsigned int milliseconds){
	shmP->clockMilliseconds += milliseconds;
	
	while (shmP->clockMilliseconds >= 1000){
		shmP->clockMilliseconds -= 1000;
		shmP->clockSeconds += 1;
	}
	
	shmP->clockSeconds += seconds;
}

