/*
$Author: o-moring $
$Log: user.c,v $
Revision 1.2  2015/04/16 11:57:07  o-moring
final

$Date: 2015/04/16 11:57:07 $
*/

/******************/
/** Derek Moring **/
/**   Project 5  **/
/**    user.c    **/
/******************/
#include <stdio.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include "shared.h"
#include "user.h"


int key;
sharedStruct *shmP;

int main(int argc, char **argv){
	int i, j, p, cont, die, random; 
	srand(time(NULL));
	
	if (argc != 2){
		perror("Invalid arguments.\n");
		exit(1);
	}
	
	p = atoi(argv[1]); // ith process
	
	key = ftok("./ftokFile", 42);
	if (key == -1){
		perror("ftok failed\n");
		return -1;
	}
	
	int shmid = shmget(key, sizeof(sharedStruct), 0777);
	
	if (shmid == -1){
		perror("Error in child shmget");
		exit(1);
	}
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
	
	// Save the time the user process starts
	shmP->proc[p].startSeconds = shmP->clockSeconds;
	shmP->proc[p].startMilliseconds = shmP->clockMilliseconds;

	// Create the max claims 
	for (i = 0; i < RESOURCES; i++){
		j = i;
		int resMax = shmP->descriptor.resource[j]; // Get the max amt of resources
		shmP->proc[p].max[i] = rand() % (resMax + 1); // claim a random amt
	}
	
	// Create the alloc and need tables
	for (i = 0; i < RESOURCES; i++){
		shmP->proc[p].alloc[i] = rand() % (shmP->proc[p].max[i] + 1); //rand num for alloc table
		shmP->proc[p].need[i] = shmP->proc[p].max[i] - shmP->proc[p].alloc[i];
	}
	
	// Decide to die/infinite loop
	die = 0;	
	cont = 1;
	while(cont){
		if (shmP->clockSeconds > shmP->proc[p].startSeconds
				&& shmP->clockMilliseconds >= shmP->proc[p].startMilliseconds){
			random = rand() % 251;
			incrementTime(0, random);
			die = rand() % 2;
			if (die){
				// do things to die.
				shmP->proc[p].EMPTY_PROCESS_SLOT = 1; 
				shmP->proc[p].FLAG_OSS = 0;
				shmP->proc[p].startSeconds = 0;
				shmP->proc[p].startMilliseconds = 0;
				shmP->processes--;
				exit(1);
			}
			else {
				shmP->proc[p].FLAG_OSS = 1;
			}
		} 
		cont = 0;
	}
}
